/*******************************************************************************
* Copyright 2016 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

#ifndef DYNAMIXEL_2_ARDUINO_H_
#define DYNAMIXEL_2_ARDUINO_H_

#include <cstdint>
#include "packet_handler.hpp"
#include "port_handler.hpp"

#define OPERATING_MODE_VELOCITY 1
#define OPERATING_MODE_POSITION 3


class Dynamixel {
private:
  dynamixel::PortHandler *port;
  dynamixel::PacketHandler *packet;

  bool CHECK_ERROR(int comm_result);

public:
  /**
   * @brief The constructor.
   * @code
   * Dynamixel dxl("UART1");
   * @endcode
   * @param port The serial interface, one of UART0, UART1 or UART2
   */   
  Dynamixel(const char *serial);

  ~Dynamixel();

  /**
   * @brief Initialization function to start communication with DYNAMIXEL.
   *      Or change baudrate of baud serial port.
   * @code
   * Dynamixel dxl("UART1");
   * dxl.begin(57600);
   * @endcode
   * @param baud The port speed you want on the board (the speed to communicate with DYNAMIXEL)
   * @return It returns true(1) on success, false(0) on failure.
   */    
  bool begin(int baud);

  /**
   * @brief It is API for getting serial baudrate of board port.
   * @code
   * Dynamixel dxl("UART1");
   * Serial.print(dxl.getPortBaud());
   * @endcode
   * @return It returns serial baudrate of board port.
   */   
  int getPortBaud();

  /**
   * @brief It is API for To checking the communication connection status of DYNAMIXEL.
   * @code
   * Dynamixel dxl("UART1");
   * dxl.ping(1);
   * @endcode
   * @param id DYNAMIXEL Actuator's ID or BROADCAST ID (0xFE). (default : 0xFE)
   * @return It returns true(1) on success, false(0) on failure.
   */
  bool ping(uint8_t id);


  /**
   * @brief It is API for controlling torque on/off(turn on) of DYNAMIXEL.
   * @code
   * Dynamixel dxl("UART1");
   * dxl.torqueOn(1);
   * @endcode
   * @param id DYNAMIXEL Actuator's ID.
   * @return It returns true(1) on success, false(0) on failure.
   */
  bool torqueOn(uint8_t id);

  /**
   * @brief It is API for controlling torque on/off(turn on) of DYNAMIXEL.
   * @code
   * Dynamixel dxl("UART1");
   * dxl.torqueOff(1);
   * @endcode
   * @param id DYNAMIXEL Actuator's ID.
   * @return It returns true(1) on success, false(0) on failure.
   */    
  bool torqueOff(uint8_t id);

  /**
   * @brief It is API for controlling torque on/off(turn on) of DYNAMIXEL.
   * @code
   * Dynamixel dxl("UART1");
   * dxl.setTorqueEnable(1, true);
   * @endcode
   * @param id DYNAMIXEL Actuator's ID.
   * @param enable torque on/off
   * @return It returns true(1) on success, false(0) on failure.
   */   
  bool setTorqueEnable(uint8_t id, bool enable);


  /**
   * @brief It is API for controlling operating mode of DYNAMIXEL.
   * @code
   * Dynamixel dxl("UART1");
   * dxl.setOperatingMode(1, OPERATING_MODE_POSITION);
   * @endcode
   * @param id DYNAMIXEL Actuator's ID.
   * @param mode The operating mode you want.
   * @return It returns true(1) on success, false(0) on failure.
   */
  bool setOperatingMode(uint8_t id, uint8_t mode);

  /**
   * @brief It is API for controlling position(joint) of DYNAMIXEL.
   * @code
   * Dynamixel dxl("UART1");
   * dxl.setGoalPosition(1, 1.57);
   * @endcode
   * @param id DYNAMIXEL Actuator's ID.
   * @param angle An angle between 0 and 2.PI radians
   * @return It returns true(1) on success, false(0) on failure.
   */
  bool setGoalPosition(uint8_t id, float angle);

  /**
   * @brief It is API for getting present position (joint) of DYNAMIXEL.
   * @code
   * Dynamixel dxl("UART1");
   * Serial.print(dxl.getPresentPosition(1));
   * @endcode
   * @param id DYNAMIXEL Actuator's ID.
   * @return It returns the data read from DXL control table item.(Returns the value in radians.)
   * If the read fails, -1.0 is returned.
   */    
  float getPresentPosition(uint8_t id);

  /**
   * @brief It is API for controlling velocity(wheel mode speed) of DYNAMIXEL.
   * @code
   * Dynamixel dxl("UART1");
   * dxl.setGoalVelocity(1, 512);
   * @endcode
   * @param id DYNAMIXEL Actuator's ID.
   * @param percent sets the disired RPM
   * @return It returns true(1) on success, false(0) on failure.
   */
  bool setGoalVelocity(uint8_t id, float percent);

  /**
   * @brief It is API for getting present velocity(wheel mode speed) of DYNAMIXEL.
   * @code
   * Dynamixel dxl("UART1");
   * Serial.print(dxl.getPresentVelocity(1));
   * @endcode
   * @param id DYNAMIXEL Actuator's ID.
   * @return It returns the data read from DXL control table item.(Returns the value in percent)
   * If the read fails, -1.0 is returned.
   */    
  float getPresentVelocity(uint8_t id);
    

  /**
   * @brief It is API for getting the torque enabled / disabled status of DYNAMIXEL.
   * @code
   * Dynamixel dxl("UART1");
   * Serial.print(dxl.getTorqueEnableStat(1));
   * @endcode
   * @param id DYNAMIXEL Actuator's ID.
   * @return It returns the Torque Enable data read from DXL control table item.
   * If the Torque is On, true(1) is returned. Otherwise false(0) is returned.
   * If the read fails, -1 is returned.
   */  
  int8_t getTorqueEnableStat(uint8_t id);   
};

#endif /* DYNAMIXEL_2_ARDUINO_H_ */
