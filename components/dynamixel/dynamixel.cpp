/*******************************************************************************
* Copyright 2016 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

#include "dynamixel.hpp"
#include <cstdint>

// #define XM430_W210_R
// #define XL430_W250_T

#define ADDR_TORQUE_ENABLE      64    // 1 byte
#define ADDR_OPERATING_MODE     11    // 1 byte
#define ADDR_GOAL_VELOCITY      104   // 4 bytes
#define ADDR_GOAL_POSITION      116   // 4 bytes
#define ADDR_PRESENT_VELOCITY   128   // 4 bytes
#define ADDR_PRESENT_POSITION   132   // 4 bytes

int dxl_comm_result = COMM_TX_FAIL;
uint8_t dxl_error = 0;

bool Dynamixel::CHECK_ERROR(int dxl_comm_result) {
  if (dxl_comm_result != COMM_SUCCESS) {
    printf("%s\n", packet->getTxRxResult(dxl_comm_result));
    return false;
  }
  else if (dxl_error != 0) {
    printf("%s\n", packet->getRxPacketError(dxl_error));
    return false;
  }
  return true;
}

Dynamixel::Dynamixel(const char *serial) {
  port = new dynamixel::PortHandler(serial);
  packet = dynamixel::PacketHandler::getPacketHandler(2.0);
}

Dynamixel::~Dynamixel() {
  port->closePort();
  delete port;
}

bool Dynamixel::begin(int baud) {
  return port->setBaudRate(baud);
}

int Dynamixel::getPortBaud() {
  return port->getBaudRate();
}

bool Dynamixel::ping(uint8_t id) {
  return CHECK_ERROR(packet->ping(port, id, &dxl_error));
}

bool Dynamixel::torqueOn(uint8_t id) {
  return setTorqueEnable(id, true);
}

bool Dynamixel::torqueOff(uint8_t id) {
  return setTorqueEnable(id, false);
}

bool Dynamixel::setTorqueEnable(uint8_t id, bool enable) {
  return CHECK_ERROR(packet->write1ByteTxRx(port, id, ADDR_TORQUE_ENABLE, enable, &dxl_error));
}

bool Dynamixel::setOperatingMode(uint8_t id, uint8_t mode) {
  return CHECK_ERROR(packet->write1ByteTxRx(port, id, ADDR_OPERATING_MODE, mode, &dxl_error));
}

bool Dynamixel::setGoalPosition(uint8_t id, float angle) {
  uint32_t value = (4096.0 / 2.0 * 3.141592654) * angle;

  return CHECK_ERROR(packet->write4ByteTxRx(port, id, ADDR_GOAL_POSITION, value, &dxl_error));
}

float Dynamixel::getPresentPosition(uint8_t id) {
  uint32_t value = 0;
  if (CHECK_ERROR(packet->read4ByteTxRx(port, id, ADDR_PRESENT_POSITION, &value, &dxl_error))) {
    return -1.0;
  }
  return (2.0 * 3.141592654 / 4096.0) * value;
}

bool Dynamixel::setGoalVelocity(uint8_t id, float percent) {
  uint32_t value = (1024.0 / 100.0) * percent;

  return CHECK_ERROR(packet->write4ByteTxRx(port, id, ADDR_GOAL_VELOCITY, value, &dxl_error));
}

float Dynamixel::getPresentVelocity(uint8_t id) {
  uint32_t value = 0;
  if (CHECK_ERROR(packet->read4ByteTxRx(port, id, ADDR_PRESENT_VELOCITY, &value, &dxl_error))) {
    return -1.0;
  }
  return (100.0 / 1024.0) * value;
}

int8_t Dynamixel::getTorqueEnableStat(uint8_t id) {
  uint8_t value = 0;
  if (CHECK_ERROR(packet->read1ByteTxRx(port, id, ADDR_PRESENT_VELOCITY, &value, &dxl_error))) {
    return -1;
  }
  return value;
}
