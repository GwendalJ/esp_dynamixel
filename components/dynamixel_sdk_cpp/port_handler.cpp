/*******************************************************************************
* Copyright 2017 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Author: zerom, Ryu Woon Jung (Leon) */

#include "port_handler.hpp"

#include <cstdio>
#include <cstring>
#include "driver/gpio.h"
#include "esp_timer.h"


#define LATENCY_TIMER  4
#define DEFAULT_BAUDRATE 57600

#define PIN_UART0_TX  1
#define PIN_UART0_RX  3
#define PIN_UART1_TX  29
#define PIN_UART1_RX  28
#define PIN_UART2_TX  17
#define PIN_UART2_RX  16

#define DIR_PIN GPIO_NUM_27

using namespace dynamixel;

PortHandler::PortHandler(const char *port_name)
  : baudrate_(DEFAULT_BAUDRATE),
    packet_start_time_(0.0),
    packet_timeout_(0.0),
    tx_time_per_byte(0.0)
{
  is_using_ = false;
  setPortName(port_name);
}

bool PortHandler::openPort()
{
  return setBaudRate(baudrate_);
}

void PortHandler::closePort()
{
  uart_driver_delete(esp_port_);
}

void PortHandler::clearPort()
{
  uart_flush(esp_port_);
}

void PortHandler::setPortName(const char *port_name)
{
  strcpy(port_name_, port_name);

  if (0 == strcmp("UART0", port_name)) {
    esp_port_ = UART_NUM_0;
    tx_pin_ = PIN_UART0_TX;
    rx_pin_ = PIN_UART0_RX;
  }
  else if (0 == strcmp("UART1", port_name)) {
    esp_port_ = UART_NUM_1;
    tx_pin_ = PIN_UART1_TX;
    rx_pin_ = PIN_UART1_RX;
  }
  else if (0 == strcmp("UART2", port_name)) {
    esp_port_ = UART_NUM_2;
    tx_pin_ = PIN_UART2_TX;
    rx_pin_ = PIN_UART2_RX;
  }
}

char *PortHandler::getPortName()
{
  return port_name_;
}

// TODO: baud number ??
bool PortHandler::setBaudRate(const int baudrate)
{
  closePort();

  baudrate_ = baudrate;
  return setupPort(baudrate);
}

int PortHandler::getBaudRate()
{
  return baudrate_;
}

int PortHandler::getBytesAvailable()
{
  size_t bytes_available;
  if (ESP_OK != uart_get_buffered_data_len(esp_port_, &bytes_available)) {
    printf("Error on uarg_get_buffered_data_len");
    bytes_available = 0;
  }
  return bytes_available;
}

int PortHandler::readPort(uint8_t *packet, int length)
{
  int len = uart_read_bytes(esp_port_, packet, length, 150);
  if (len < 0) {
    printf("readPortEspIdf: uart_read_bytes returned error, clamped to 0");
    len = 0;
  }
  return len;
}

int PortHandler::writePort(uint8_t *packet, int length)
{
  gpio_set_level(DIR_PIN, 1);

  int len = uart_write_bytes(esp_port_, (const char*)packet, length);
  if (len < 0) {
    printf("writePortEspIdf: uart_write_bytes returned %d unexpected", len);
    len = 0;
  }

  gpio_set_level(DIR_PIN, 0);

  return len;
}

void PortHandler::setPacketTimeout(uint16_t packet_length)
{
  packet_start_time_  = getCurrentTime();
  packet_timeout_     = (tx_time_per_byte * (double)packet_length) + (LATENCY_TIMER * 2.0) + 2.0;
}

void PortHandler::setPacketTimeout(double msec)
{
  packet_start_time_  = getCurrentTime();
  packet_timeout_     = msec;
}

bool PortHandler::isPacketTimeout()
{
  if(getTimeSinceStart() > packet_timeout_)
  {
    packet_timeout_ = 0;
    return true;
  }
  return false;
}

double PortHandler::getCurrentTime()
{
  return esp_timer_get_time() / 1000.0;
}

double PortHandler::getTimeSinceStart()
{
  double time;

  time = getCurrentTime() - packet_start_time_;
  if(time < 0.0)
    packet_start_time_ = getCurrentTime();

  return time;
}

bool PortHandler::setupPort(int baud)
{
  uart_config_t uart_config = {
    .baud_rate = baudrate_,
    .data_bits = UART_DATA_8_BITS,
    .parity    = UART_PARITY_DISABLE,
    .stop_bits = UART_STOP_BITS_1,
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    .rx_flow_ctrl_thresh = 0,
    .source_clk = UART_SCLK_APB,
  };

  ESP_ERROR_CHECK(uart_driver_install(esp_port_, 512, 0, 0, NULL, 0));
  ESP_ERROR_CHECK(uart_param_config(esp_port_, &uart_config));
  ESP_ERROR_CHECK(uart_set_pin(esp_port_, tx_pin_, rx_pin_, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE));

  tx_time_per_byte = (1000.0 / (double)baudrate_) * 10.0;


  gpio_reset_pin(DIR_PIN);
  gpio_set_direction(DIR_PIN, GPIO_MODE_OUTPUT);
  gpio_set_pull_mode(DIR_PIN, GPIO_PULLDOWN_ONLY);
  gpio_set_level(DIR_PIN, 0);

  return true;
}
