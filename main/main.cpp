#include <dynamixel.hpp>

#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "portmacro.h"


#define DXL_SERIAL_PORT   "UART2"
#define DXL_ID            1


extern "C" void app_main() {
  Dynamixel dxl(DXL_SERIAL_PORT);

  dxl.begin(57600);

  dxl.torqueOff(DXL_ID);
  dxl.setOperatingMode(DXL_ID, OPERATING_MODE_POSITION);
  dxl.torqueOn(DXL_ID);

  while (true) {
    dxl.setGoalPosition(DXL_ID, 0);
    vTaskDelay(1000 / portTICK_PERIOD_MS);

    dxl.setGoalPosition(DXL_ID, 1.8); // ~90°
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}
